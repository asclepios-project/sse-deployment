tosca_definitions_version: tosca_simple_yaml_1_2                                    

imports:
  - https://raw.githubusercontent.com/micado-scale/tosca/develop/micado_types.yaml

repositories:
  docker_hub: https://hub.docker.com/

description: ADT for SSE on EC2

dsl_definitions:
  compute_cloud: &compute_cloud
    type: tosca.nodes.MiCADO.EC2.Compute
    properties:
      region_name: eu-west-2
      image_id: ami-0194c3e07668a7e36
      instance_type: t2.small
      security_group_ids:
        - sg-019d99c555ff2d5aa
      key_name: YangCPC
    interfaces:
      Occopus:
        create:
          inputs:
            endpoint: https://ec2.eu-west-2.amazonaws.com


topology_template:
  node_templates:

    traefik:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        image: traefik:1.7
        ports:
        - containerPort: 80
          hostPort: 80
        - containerPort: 443
          hostPort: 443
        args:
        - --api
        - --kubernetes
        - --logLevel=INFO
        - --defaultentrypoints=http,https
        - --entrypoints=Name:http Address::80 Redirect.EntryPoint:https
        - --entrypoints=Name:https Address::443 TLS
      requirements:
      - host: sse-node
      interfaces:
        Kubernetes:
          create:
            inputs:
              metadata:
                namespace: kube-system
              spec:
                template:
                  spec:
                    serviceAccountName: traefik-ingress
                    terminationGracePeriodSeconds: 60

    issuer:
      type: tosca.nodes.MiCADO.Kubernetes
      interfaces:
        Kubernetes:
          create:
            inputs:
              apiVersion: cert-manager.io/v1
              kind: ClusterIssuer
              metadata:
                name: letsencrypt
              spec:
                acme:
                  server: https://acme-v02.api.letsencrypt.org/directory
                  email: y.ma1@westminster.ac.uk
                  privateKeySecretRef:
                    name: letsencrypt
                  solvers:
                  - http01:
                      ingress:
                        class: traefik

    certificate:
      type: tosca.nodes.MiCADO.Kubernetes
      interfaces:
        Kubernetes:
          create:
            inputs:
              apiVersion: cert-manager.io/v1
              kind: Certificate
              metadata:
                name: test1.yangcpc.live
              spec:
                secretName: test1.yangcpc.live-tls
                issuerRef:
                  name: letsencrypt
                  kind: ClusterIssuer
                commonName: test1.yangcpc.live
                dnsNames:
                - test1.yangcpc.live

    ingress:
      type: tosca.nodes.MiCADO.Kubernetes
      interfaces:
        Kubernetes:
          create:
            inputs:
              apiVersion: extensions/v1beta1
              kind: Ingress
              metadata:
                name: traefik-ingress
                annotations:
                  nginx.ingress.kubernetes.io/enable-cors: "true"
                  nginx.ingress.kubernetes.io/cors-allow-methods: "PUT, GET, POST, OPTIONS"
                  nginx.ingress.kubernetes.io/cors-allow-origin: "*"
                  kubernetes.io/ingress.class: traefik
                  cert-manager.io/cluster-issuer: letsencrypt
              spec:
                rules:
                - host: test1.yangcpc.live
                  http:
                    paths:
                    - backend:
                        serviceName: ta
                        servicePort: 8000
                tls:
                - hosts:
                  - test1.yangcpc.live
                  secretName: test1.yangcpc.live-tls


    # Describe sse containers
    sse-client: # replace with own client
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        name: sse-client
        image: uowcpc/asclepios-client
        ports:
        - containerPort: 80
          hostPort: 10000
        - port: 80
        env:
        - name: ALLOWED_HOSTS
          value: "*"   #${SSE_CLIENT_ALLOWED_HOSTS}
        - name: DJANGO_DEBUG
          value: "True"   #${SSE_CLIENT_DJANGO_DEBUG}
        - name: DJANGO_SECRET_KEY
          value: "9qZa2o2*9wq5@42qy!0n$j@cjm7j2mn)zby%28tt%6jeou!yeq"   #${SSE_CLIENT_DJANGO_SECRET_KEY}
        - name: TA_URL
          value: "https://test1.yangcpc.live:443"   #${SSE_CLIENT_TA_URL}
        - name: SSE_URL
          value: "http://52.56.58.149:8080"   #${SSE_CLIENT_SSE_URL}
        - name: SALT
          value: "ZWdhYndlZmc="   #${SSE_CLIENT_SALT}
        - name: IV
          value: "bGd3YmFnd2c="   #${SSE_CLIENT_IV}
        - name: ITER
          value: "10000"   #${SSE_CLIENT_ITER}
        - name: KS
          value: "256"   #${SSE_CLIENT_KS}
        - name: TS
          value: "64"   #${SSE_CLIENT_TS}
        - name: MODE
          value: "ccm"   #${SSE_CLIENT_MODE}
        - name: ADATA
          value: ""   #${SSE_CLIENT_ADATA}
        - name: ADATA_LEN
          value: "0"   #${SSE_CLIENT_ADATA_LEN}
        - name: HASH_LEN
          value: "256"   #${SSE_CLIENT_HASH_LEN}
        - name: CHUNK_SIZE
          value: "32768"   #${SSE_CLIENT_CHUNK_SIZE}
        - name: NO_CHUNKS_PER_UPLOAD
          value: "30"   #${SSE_CLIENT_NO_CHUNKS_PER_UPLOAD}
        - name: SALT_TA
          value: "ZRVja4LFrFY="   #${SSE_CLIENT_SALT_TA}
        - name: IV_TA
          value: "YXp5bWJscWU="   #${SSE_CLIENT_IV_TA}
        - name: ITER_TA
          value: "10000"   #${SSE_CLIENT_ITER_TA}
        - name: KS_TA
          value: "128"   #${SSE_CLIENT_KS_TA}
        - name: TS_TA
          value: "64"   #${SSE_CLIENT_TS_TA}
        - name: MODE_TA
          value: "ccm"   #${SSE_CLIENT_MODE_TA}
        - name: ADATA_TA
          value: ""   #${SSE_CLIENT_ADATA_TA}
        - name: DATA_LEN_TA
          value: "0"   #${SSE_CLIENT_ADATA_LEN_TA}
        - name: SGX_ENABLE
          value: "false"   #${SSE_CLIENT_SGX_ENABLE}
        - name: CP_ABE_URL
          value: "https://asclepios-cpabe.euprojects.net"   #${SSE_CLIENT_CP_ABE_URL}
        - name: DEBUG
          value: "true"   #${SSE_CLIENT_DEBUG}
        - name: AUTH
          value: "false"   #${SSE_CLIENT_AUTH}
        - name: SMALL_FILE
          value: "3072"   #${SSE_CLIENT_SMALL_FILE}
      requirements:
      - host: sse-node
      
    sse:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        name: sse
        image: registry.gitlab.com/asclepios-project/symmetric-searchable-encryption-server:0.6
        ports:
        - containerPort: 8080
          hostPort: 8080
        - port: 8080
        env:
        - name: DJANGO_LOGLEVEL
          value: "DEBUG"   #${SSE_SERVER_DJANGO_LOGLEVEL}
        - name: ALLOWED_HOSTS
          value: "*"   #${SSE_SERVER_ALLOWED_HOSTS}
        - name: DJANGO_DEBUG
          value: "true"   #${SSE_SERVER_DJANGO_DEBUG}
        - name: DJANGO_SECRET_KEY
          value: "kl#rqhxq^m8s@vcve3o2-r7@vun^)=&8o+h@15+n9m-*6_v6k)"   #${SSE_SERVER_DJANGO_SECRET_KEY}
        - name: TA_SERVER
          value: "http://ta:8000"   #${SSE_SERVER_TA_SERVER}
        - name: DB_NAME
          value: "ssedb"   #${SSE_SERVER_DB_NAME}
        - name: DB_USER
          value: "sseadmin"   #${SSE_SERVER_DB_USER}
        - name: DB_PASSWORD
          value: "sseadmin"   #${SSE_SERVER_DB_PASSWORD}
        - name: DB_HOST
          value: "sse-db"   #${SSE_SERVER_DB_HOST}
        - name: DB_PORT
          value: "5432"   #${SSE_SERVER_DB_PORT}
        - name: MINIO_ACCESS_KEY
          value: "minio"   #${MINIO_ACCESS_KEY}
        - name: MINIO_SECRET_KEY
          value: "minio123"   #${MINIO_SECRET_KEY}
        - name: MINIO_BUCKET_NAME
          value: "asclepios"   #${MINIO_BUCKET_NAME}
        - name: MINIO_URL
          value: "minio:9000"   #${MINIO_URL}
        - name: MINIO_SSL_SECURE
          value: "false"   #${MINIO_SSL_SECURE}
        - name: MINIO_EXPIRE_GET
          value: "1"   #${MINIO_EXPIRE_GET}
        - name: MINIO_EXPIRE_PUT
          value: "1"   #${MINIO_EXPIRE_PUT}
      requirements:
      - host: sse-node

    sse-db:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        name: sse-db
        image: postgres
        ports:
        - port: 5432
        env:
        - name: POSTGRES_USER
          value: "sseadmin"   #${SSE_SERVER_DB_USER}
        - name: POSTGRES_PASSWORD
          value: "sseadmin"   #${SSE_SERVER_DB_PASSWORD}
        - name: POSTGRES_DB
          value: "ssedb"   #${SSE_SERVER_DB_NAME}
        - name: PGDATA
          value: /var/lib/postgresql/data
      requirements:
      - host: sse-node
      - volume:
          node: sse-db-volume
          relationship:
            type: tosca.relationships.AttachesTo
            properties:
              location: /var/lib/postgresql/data

    sse-db-volume:
      type: tosca.nodes.MiCADO.Container.Volume.HostPath
      properties:
        path: /var/lib/data/sse-db      #./data/sse-db:/var/lib/postgresql/data

 
    # Describe the TA containers
    ta:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        name: ta
        image: registry.gitlab.com/asclepios-project/sseta:0.5
        ports:
        - containerPort: 8000
          hostPort: 8000
        - port: 8000
        env:
        - name: DJANGO_LOGLEVEL
          value: "DEBUG"  #${TA_DJANGO_LOGLEVEL}
        - name: DJANGO_DEBUG
          value: "true"   #${TA_DJANGO_DEBUG} 
        - name: DJANGO_SECRET_KEY
          value: "6jpu71#_4j7jaorh+_llj35p$gno7@)+!04n!#q_27b+4cv%5)"  #${TA_DJANGO_SECRET_KEY}
        - name: DB_NAME
          value: "tadb"   #${TA_DB_NAME}
        - name: DB_USER     
          value: "taadmin"   #${TA_DB_USER}
        - name: DB_PASSWORD
          value: "taadmin"   #${TA_DB_PASSWORD}
        - name: DB_HOST
          value: "ta-db"   #${TA_DB_HOST}
        - name: DB_PORT    
          value: "5432"   #${TA_DB_PORT}
        - name: ALLOWED_HOSTS
          value: "*"   #${TA_ALLOWED_HOSTS}
        - name: HASH_LENGTH
          value: "256"   #${TA_HASH_LENGTH}
        - name: IV  
          value: "azymblqe"   #${TA_IV}
        - name: MODE          
          value: "ccm"   #${TA_MODE}
        - name: KS
          value: "128"   #${TA_KS}
        - name: TEEP_SERVER  
#          value: "coap://teep-server:5683/teep"   #${TA_TEEP_SERVER}
          value: "TA_TEEP_SERVER"
        - name: SGX  
          value: "0"   #${TA_SGX}
      requirements:
      - host: sse-node    

    ta-db:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        name: ta-db
        image: postgres
        ports:
        - port: 5432
        env:
        - name: POSTGRES_USER
          value: "taadmin"   #${TA_DB_USER}
        - name: POSTGRES_PASSWORD
          value: "taadmin"   #${TA_DB_PASSWORD}
        - name: POSTGRES_DB
          value: "tadb"   #${TA_DB_NAME}
        - name: PGDATA    
          value: /var/lib/postgresql/data
      requirements:
      - host: sse-node
      - volume: 
          node: ta-db-volume
          relationship:
            type: tosca.relationships.AttachesTo
            properties:
              location: /var/lib/postgresql/data

    ta-db-volume:
      type: tosca.nodes.MiCADO.Container.Volume.HostPath
      properties:
        path: /var/lib/data/ta-db   #./data/ta-db:/var/lib/postgresql/data

        
#    teep-server:
#      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
#      properties:
#        name: teep-server
#        image: registry.gitlab.com/asclepios-project/teep-deployer
#        ports: 
#         - containerPort: 5683
#           hostPort: 5683

    # minio container
    minio:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        name: minio
        image: minio/minio
        ports:
        - containerPort: 9000
          hostPort: 9000
        command:
           - /bin/bash
           - -c
           - minio server /data
        env:
        - name: MINIO_ACCESS_KEY
          value: "minio"   #${MINIO_ACCESS_KEY}
        - name: MINIO_SECRET_KEY
          value: "minio123"   #${MINIO_SECRET_KEY}
      requirements:
      - host: sse-node
      - volume:
          node: minio-volume
          relationship:
            type: tosca.relationships.AttachesTo
            properties:
              location: /data
    
    minio-volume:
      type: tosca.nodes.MiCADO.Container.Volume.HostPath
      properties:
        path: /var/lib/data/minio   #./data/minio:/data

    # Describe server VMs
    sse-node: *compute_cloud

  policies:
  - monitoring:
      type: tosca.policies.Monitoring.MiCADO
      properties:
        enable_container_metrics: true
        enable_node_metrics: true







