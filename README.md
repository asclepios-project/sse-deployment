# Asclepios-SSE-Deployment
## Introduction 

The Symmetric Searchable Encryption (SSE) consists of three main parties: the SSE server, the SSE Trusted Authority and a demo SSE client. In addition, MinIO server and teep-server are optional components for SSE. 

More specially, SSE client is a web application which uses SSE services such as uploading encrypted data, search/update/delete over the stored encrypted data. SSE server stores the encrypted data which is uploaded by the SSE client. It also provides the services such as search/update/delete over the store data. SSE Trusted Authority (SSE TA) stores the metadata which is used for generating the search/update/delete token (at the SSE client), and verifying the search/update/delete tokens on behalf of the SSE server. Apart from that, MinIO server stores encrypted binary large objects (blob), and teep-server provides SGX related services such as creating, installing SGX enclaves. These two components are optional. MinIO is only necessary when a client application wishes to support encrypting blob data and search/update/delete over its encrypted metadata. Meanwhile, teep-server is needed when we wish to enable SGX at SSE TA to increase the security level.

## Deployment SSE service with docker-compose 

It is possible to deploy all the SSE components on a single host using docker-compose. The following section describes how to do that.

**1. Get the docker-compose definition from the repository**

```bash
git clone https://gitlab.com/asclepios-project/sse-deployment
```

Two docker-compose files could be achieved, namely "docker-compose.yml" and "docker-compose-tls.yml".   

The "docker-compose.yml" file involves the definitions of the SSE server, SSE TA, SSE client, MinIO Server, and TEEP deployer server. While, the "docker-compose-tls.yml" file involves the definitions of the SSE server, SSE TA, SSE client, certbot, ta-proxy (Nginx), MinIO Server, and TEEP deployer server. If you do not need to use SGX for SSE TA, and/or use MinIO Server, you can customize the docker-compose file by removing teep-server and/or minio definitions. 

The main difference between these two files is that "docker-compose-tls.yml" adds a TLS layer in order to increase security level on SSE TA. 

**2. Configure the environmental variables**

```bash
cd sse-deployment
```

If using SGX at SSE TA:

```bash
cp template_sgx.env .env
```

If not using SGX at SSE TA:

```bash
cp template_nonsgx.env .env
```

Edit the ``.env`` file to modify the configurations and more details could be found in [SSE_deployment_manual.pdf](https://gitlab.com/asclepios-project/ssemanual/-/blob/develop/SSE_deployment_manual.pdf).

**3. Build the docker images**

```bash
sudo docker-compose build
```

The following part will describe how to run SSE service with "docker-compose.yml" and "docker-compose-tls.yml" separately. 

### Run SSE service with docker-compose-without-tls.yml

**4. Run SSE service**

Rename ‘docker-compose-without-tls.yml’ to ‘docker-compose.yml’

```bash
mv docker-compose-without-tls.yml docker-compose.yml
```

Run the SSE service

```bash
docker-compose up
```

### Run SSE service with docker-compose-tls.yml

**4. Update the certbot entry in ‘docker-compose-tls.yml’ with email and domain**

First, edit ‘docker-compose-tls.yml’ with your favourite editor

```bash
nano docker-compose-tls.yml
```

Scroll down to certbot and modify the --*email* and *-d* parameters. Save the file. *(XXX is your email address and YYY is the TA_DOMAIN_NAME)*

```bash
--email XXX 
-d YYY
```

Rename ‘docker-compose-tls.yml’ to ‘docker-compose.yml’

```bash
mv docker-compose-tls.yml docker-compose.yml
```

**5. Update the 'nginx.http' file with the domain name *(YYY is the TA_DOMAIN_NAME)*** 

```bash
nano nginx.http 

server_name YYY
```

Rename ‘nginx.http’ to ‘nginx.conf’

```bash
mv nginx.http nginx.conf
```

**6. Run the SSE service**

```bash
docker-compose up -d
```

**7. Update the ‘nginx.https’ file with the domain name *(YYY is the TA_DOMAIN_NAME)***

```bash
nano nginx.https

server_name YYY 
/etc/letsencrypt/live/YYY/fullchain.pem; 
/etc/letsencrypt/live/YYY/privkey.pem;
```

Rename ‘nginx.conf’ to ‘nginx.http’

```bash
mv nginx.conf nginx.http
```

Rename ‘nginx.https’ to ‘nginx.conf’

```bash
mv nginx.https nginx.conf
```

**8. Restart the Nginx service**

```bash
docker-compose restart ta-proxy
```

## Deployment SSE service using MiCADO

### Preliminaries

- MiCADO installation

The details of MiCADO installation can be found in https://micado-scale.readthedocs.io/en/develop/deployment.html, but two steps below need to be updated during the installation.

i. Download the ansible playbook from the develop branch

```bash
git clone https://github.com/micado-scale/ansible-micado
cd ansible-micado
git checkout develop
```

ii. ‘cert-manager.yaml’ and ‘traefik-rbac.yaml’ need to be installed on the MiCADO master before the deployment.

(1) SSH to MiCADO master node.

(2) Cope ‘cert-manager.yaml’ and ‘traefik-rbac.yaml’ files to any directory in MiCADO master node.

(3) Run the commands below.

```bash
sudo kubectl apply -f cert-manager.yaml
sudo kubectl apply -f traefik-rbac.yaml
```

### Deployment using MiCADO

Four ADT templates (below) could be found in the main branch of [sse-deployment](https://gitlab.com/asclepios-project/sse-deployment/-/tree/develop1).

- Template_SSE_without_TLS_AWS.yaml
- Template_SSE_without_TLS_Azure.yaml
- Template_SSE_with_TLS_AWS.yaml
- Template_SSE_with_TLS_Azure.yaml

The next step is to configure the ADT file and then run the command on Ansible Remote machine to deploy the SSE service (Some tutorials of how to deploy applications on MiCADO could be find in [Tutorials](https://micado-scale.readthedocs.io/en/latest/tutorials.html)).

For further details, you can also read about the SSE service deployment in [SSE_deployment_manual.pdf](https://gitlab.com/asclepios-project/ssemanual/-/blob/develop/SSE_deployment_manual.pdf).

**P.S.** The SSE ADT examples and SSE service test reports could be found in the "ADT examples" folder of [sse-deployment](https://gitlab.com/asclepios-project/sse-deployment/-/tree/develop1).

