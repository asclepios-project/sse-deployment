#!/bin/bash

COMPOSE="/usr/local/bin/docker-compose --ansi never"
DOCKER="/usr/bin/docker"

cd /home/ubuntu/sse/sse-tls-deployment
$COMPOSE run certbot renew && $COMPOSE kill -s SIGHUP ta-proxy
$DOCKER system prune -af
